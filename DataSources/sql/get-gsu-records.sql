SELECT
H.[Key] AS HKey,
H.Source AS Survey,
H.Year AS Year,
H.Month AS Month,
H.Day AS Day,
H.StatArea AS Major_stat_area_code,
H.SubArea AS Stat_subarea_code,
CASE
    WHEN H.TransOrientation = 'D' THEN H.LatDegStart + H.LatMinStart / 60
    WHEN H.TransOrientation = 'S' THEN H.LatDegEnd + H.LatMinEnd / 60
    END AS LatDeep,
CASE
    WHEN H.TransOrientation = 'D' THEN -(H.LongDegStart + H.LongMinStart / 60)
    WHEN H.TransOrientation = 'S' THEN -(H.LongDegEnd + H.LongMinEnd / 60)
    END AS LonDeep,
CASE 
    WHEN H.TransOrientation = 'D' THEN H.LatDegEnd + H.LatMinEnd / 60
    WHEN H.TransOrientation = 'S' THEN H.LatDegStart + H.LatMinStart / 60
    END AS LatShallow,
CASE 
    WHEN H.TransOrientation = 'D' THEN -(H.LongDegEnd + H.LongMinEnd / 60)
    WHEN H.TransOrientation = 'S' THEN -(H.LongDegStart + H.LongMinStart / 60)
    END AS LonShallow,
H.TransLen AS Transect_length,
IsNull(FORMAT (D.QTime,'HH:mm'),FORMAT (H.StartTime,'HH:mm')) AS Time,
H.TimeType AS Time_type,
D.QuadratNum AS Quadrat,
D.GageDepth*0.3048 AS Depth_gauge_m,
D.ChartDepth AS CorDepthM,
CASE WHEN H.Cukes = 'N' THEN 0 ELSE NULL END AS RSC_absence,
CASE WHEN H.Geoducks = 'N' THEN 0 ELSE NULL END AS GDK_absence,
CASE WHEN D.CountTotal = 0 THEN 0 WHEN D.CountTotal > 0 THEN 1 END AS GSU,
CASE WHEN D.CountReds = 0 THEN 0 WHEN D.CountReds > 0 THEN 1 END AS RSU,
CASE WHEN D.CountPurples = 0 THEN 0	WHEN D.CountPurples > 0 THEN 1 END AS PSU,
CASE 
    WHEN ',' + D.Algae + ',' LIKE '%PT%' THEN 1 
	WHEN ',' + D.Algae2 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + D.Algae3 + ',' LIKE '%PT%' THEN 1 
	WHEN ',' + D.Algae4 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.CanopySpecies1 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.CanopySpecies2 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies1 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies2 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies3 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies4 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies5 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies6 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies7 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies8 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies9 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.UndStySpecies10 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.TurfSpecies1 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.TurfSpecies2 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.TurfSpecies3 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.TurfSpecies4 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.TurfSpecies5 + ',' LIKE '%PT%' THEN 1
	WHEN ',' + A.TurfSpecies6 + ',' LIKE '%PT%' THEN 1
	ELSE 0 
	END AS PT,
CASE 
    WHEN ',' + D.Algae + ',' LIKE '%Z%' THEN 1 
	WHEN ',' + D.Algae2 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + D.Algae3 + ',' LIKE '%Z%' THEN 1 
	WHEN ',' + D.Algae4 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.CanopySpecies1 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.CanopySpecies2 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies1 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies2 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies3 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies4 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies5 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies6 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies7 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies8 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies9 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.UndStySpecies10 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.TurfSpecies1 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.TurfSpecies2 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.TurfSpecies3 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.TurfSpecies4 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.TurfSpecies5 + ',' LIKE '%Z%' THEN 1
	WHEN ',' + A.TurfSpecies6 + ',' LIKE '%Z%' THEN 1
	ELSE 0 
	END AS ZO,
CASE 
    WHEN ',' + D.Algae + ',' LIKE '%NT%' THEN 1 
	WHEN ',' + D.Algae2 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + D.Algae3 + ',' LIKE '%NT%' THEN 1 
	WHEN ',' + D.Algae4 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.CanopySpecies1 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.CanopySpecies2 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies1 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies2 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies3 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies4 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies5 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies6 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies7 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies8 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies9 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.UndStySpecies10 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.TurfSpecies1 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.TurfSpecies2 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.TurfSpecies3 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.TurfSpecies4 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.TurfSpecies5 + ',' LIKE '%NT%' THEN 1
	WHEN ',' + A.TurfSpecies6 + ',' LIKE '%NT%' THEN 1
	ELSE 0 
	END AS NT,
CASE 
    WHEN ',' + D.Algae + ',' LIKE '%MA%' THEN 1 
	WHEN ',' + D.Algae2 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + D.Algae3 + ',' LIKE '%MA%' THEN 1 
	WHEN ',' + D.Algae4 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.CanopySpecies1 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.CanopySpecies2 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies1 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies2 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies3 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies4 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies5 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies6 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies7 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies8 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies9 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.UndStySpecies10 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.TurfSpecies1 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.TurfSpecies2 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.TurfSpecies3 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.TurfSpecies4 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.TurfSpecies5 + ',' LIKE '%MA%' THEN 1
	WHEN ',' + A.TurfSpecies6 + ',' LIKE '%MA%' THEN 1
	ELSE 0 
	END AS MA
FROM Shellfish_Bio_Urchin.dbo.UrchHeaders H
    INNER JOIN 
    Shellfish_Bio_Urchin.dbo.UrchDensity D ON H.[Key] = D.HKey
    LEFT JOIN
    Shellfish_Bio_Urchin.dbo.UrchHabitat A ON H.[Key] = A.HKey AND D.QuadratNum = A.QuadratNum
WHERE H.LatDegStart IS NOT NULL AND H.LongDegStart IS NOT NULL AND H.Year > 2009 AND D.QuadratNum > 0 AND H.Species = '6BB' AND H.TransOrientation != 'U' 
ORDER BY H.Year, H.[Key], D.QuadratNum;


