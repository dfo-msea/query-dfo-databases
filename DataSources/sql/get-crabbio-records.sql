;with dataSrc as 
( 
select ROW_NUMBER() over(partition by H.[Key] order by H.[Key]) as RowNum,
H.[Key] AS HKey, 
H.[Source] AS Survey, 
H.SetNum AS Set_ID,
H.Year AS Year,
H.Month AS Month,
H.Day AS Day,
H.StatArea AS Major_stat_area_code,
H.SubArea AS Stat_subarea_code,
H.StartLatDeg + H.StartLatMin / 60 AS Start_latitude,
-(H.StartLongDeg + H.StartLongMin / 60) AS Start_longitude,
H.EndLatDeg + H.EndLatMin / 60 AS End_latitude,
-(H.EndLongDeg + H.EndLongMin / 60) AS End_longitude,
(IsNull(H.MinDepth,H.MaxDepth) + IsNull(H.MaxDepth,H.MinDepth))/2  AS Depth,
H.DepthUnit AS Depth_unit,
H.NumTrapsSampled AS Num_traps,
CASE WHEN H.NumCrabsSampled = 0 THEN 0 WHEN H.NumCrabsSampled > 0 THEN 1 END AS XKG 
from Shellfish_Bio_Crab.dbo.CrabHeaders H INNER JOIN Shellfish_Bio_Crab.dbo.CrabLF LF ON LF.HKey = H.[Key] 
WHERE H.[Source]='RL' 
    AND (LF.TrapUsability=0 OR LF.TrapUsability=15)
	AND (H.FishingMethod='G' OR H.FishingMethod='S' OR H.FishingMethod='B' OR H.FishingMethod='X')
	AND H.StartLatDeg IS NOT NULL 
	AND H.StartLongDeg IS NOT NULL) 
select HKey, Survey, Set_ID, Year, Month, Day, Major_stat_area_code, Stat_subarea_code, Start_latitude, Start_longitude, End_latitude, End_longitude, Depth, Depth_unit, Num_traps, XKG 
from dataSrc WHERE RowNum = 1
ORDER BY Year, Month, Day, HKey
;
