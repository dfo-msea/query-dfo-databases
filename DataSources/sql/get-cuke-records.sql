SELECT
H.[Key] AS HKey,
H.Source AS Survey,
H.Year AS Year,
H.Month AS Month,
H.Day AS Day,
H.StatArea AS Major_stat_area_code,
H.SubArea AS Stat_subarea_code,
H.LatDegDeep + H.LatMinDeep / 60 AS LatDeep,
-(H.LongDegDeep + H.LongMinDeep / 60) AS LonDeep,
H.LatDegShallow + H.LatMinShallow / 60 AS LatShallow,
-(H.LongDegShallow + H.LongMinShallow / 60) AS LonShallow,
H.Transect,
H.TransLen AS Transect_length,
IsNull(FORMAT (D.QTime,'HH:mm'),FORMAT (H.TimeStart,'HH:mm')) AS Time,
H.TimeType AS Time_type,
D.QuadratNum AS Quadrat,
D.Distance AS Transect_distance_from_start,
D.GaugeDepthFeet*0.3048 AS Depth_gauge_m,
D.ChartDepth AS CorDepthM,
CASE WHEN H.RSU = 'N' OR H.RSU = '0' THEN 0 END AS RSU_absence,
CASE WHEN H.GSU = 'N' OR H.GSU = '0' THEN 0 END AS GSU_absence,
CASE WHEN H.Geoducks = 'N' OR H.Geoducks = '0' THEN 0 END AS GDK_absence,
CASE WHEN (IsNull(D.CountADTotal,0) + IsNull(D.CountJuvTotal,0)) = 0 THEN 0 WHEN (IsNull(D.CountADTotal,0) + IsNull(D.CountJuvTotal,0)) > 0 THEN 1 END AS RSC,
CASE WHEN ',' + D.Algae1 + ',' LIKE '%PT%' THEN 1 WHEN ',' + D.Algae2 + ',' LIKE '%PT%' THEN 1 ELSE 0 END AS PT,
CASE WHEN ',' + D.Algae1 + ',' LIKE '%ZO%' THEN 1 WHEN ',' + D.Algae2 + ',' LIKE '%ZO%' THEN 1 ELSE 0 END AS ZO,
CASE WHEN ',' + D.Algae1 + ',' LIKE '%NT%' THEN 1 WHEN ',' + D.Algae2 + ',' LIKE '%NT%' THEN 1 ELSE 0 END AS NT,
CASE WHEN ',' + D.Algae1 + ',' LIKE '%MA%' THEN 1 WHEN ',' + D.Algae2 + ',' LIKE '%MA%' THEN 1 ELSE 0 END AS MA
FROM Shellfish_Bio_Other.dbo.SeaCukeHeaders H
    INNER JOIN 
    Shellfish_Bio_Other.dbo.SeaCukeDensities D ON H.[Key] = D.HKey
WHERE H.LatDegDeep IS NOT NULL AND H.LongDegDeep IS NOT NULL AND H.Year > 2009 AND D.QuadratNum > 0
ORDER BY H.Year, H.Transect, D.QuadratNum;
