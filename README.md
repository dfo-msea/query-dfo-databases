﻿# Presence Absence Observations of Species from DFO databases

__Main author:__  Ashley Park and Jessica Nephin    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: ashley.park@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Requirements](#requirements)
- [Methods](#methods)
  + [Shellfish Surveys](#shellfish-surveys)
  + [Groundfish Survey](#groundfish-surveys)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To produce a dataset of select shellfish, groundfish, and algae/marine plant presence and absence observation on the BC shelf and nearshore to be used to develop species distribution models for oil spill response and marine spatial planning initiatives.

## Summary
To access presence/absence data for a variety of invertebrate, groundfish, algae, and marine plants that are sampled directly (e.g. dive observations, longline, trawl) by a variety of sources within DFO Pacific (Groundfish data unit, Shellfish data unit, MSEA section).

## Status
In progress, need to include BHM databases, queries for other shellfish db (Clam Bio, scallop Bio, tanner crab bio) and gfbio db (synoptic trawl, ect) 

## Contents
This project contains code within DataSources/ sub-directories that queries groundfish and shellfish databases, runs QA/QC checks and removes errors, and creates unique fields to identify surveys and sampling events.   
These data collected by dive survey (shellfish databases) are then intended to be sent to a spatialize transects workflow (https://gitlab.com/dfo-msea/spatialise-dive-quadrats) and then all combined together before species distribution modelling (https://gitlab.com/dfo-msea/sdm).  
These data collected from the gfbio longline survey are then intended to be sent to a spatialize workflow (script in development) before species distribution modelling (https://gitlab.com/dfo-msea/sdm).

## Requirements
R software used. R packages dplyr, DBI, odbc, data.table, reshape2. Access to DFO Groundfish and Shellfish data servers as well as input data from MSEA DFO networked drives are required. 

## Methods
Seperate SQL queries have been developed to extract data from each of the different shellfish and groundfish surveys. 

### Shellfish Surveys
Northern Abalone (Haliotis kamtschatkana; ABL) data is restricted under Section 124 of the Species At Risk Act (SARA). To access and use Presence/Absence (PA) data for ABL you must first notify the SARA program and the DFO Shellfish Data Unit. To access the data the relevant SQL code must be updated for the databases.  
Crab bio: crab trap survey with presence-absence (PA) data for Dungeness crab.   
RSU bio: red sea urchin (RSU) dive surveys with PA data for RSU, green sea urchin (GSU), purple sea urchin (PSU), ABL, pterygophora kelp (PT), eelgrass species (ZO), bull kelp (NT), and giant kelp (MA). Absence only observations for California sea cucumber (RSC) and Pacific geoduck (GDK).  
GSU bio: GSU dive surveys with PA data for GSU, RSU, PSU, ABL, PT, ZO, NT, and MA. Absence only observations for RSC and GDK.   
GDK bio: GDK dive surveys with PA data for GDK, horse clam (HSC), PT, ZO, NT, and MA. Absence only observations for RSC, RSU and ABL.   
Cuke bio: RSC dive surveys with PA data for RSC, PT, ZO, NT, and MA. Absence only observations for RSU, GSU, and GDK. ABL were excluded from the absence only observations as they are often cryptic and due to the nature of sea cucumber surveys may miss their occurence.  
Multi-species bio: Multi-species dive surveys with PA data for RSC, GDK, RSU, GSU, PSU, ABL, PYN (sunflower star), PT, ZO, NT, and MA.  
Shrimp trawl bio: Multi-species small mesh (aka shrimp) trawl survey. This query extracts both fish and invertebrate observations.  

### Groundfish Surveys
Synoptic trawl surveys have been conducted since 2003 in five different locations: Queen Charlotte Sound, west coast Vancouver Island, Hecate Strait, west coast Haida Gwaii, and Strait of Georgia . This query extracts both fish and invertebrate observations.  
Long line surveys include hard bottom longline hook survey for inside and outside waters and the International Pacific Halibut Commision longline surveys. Only fish observations are queried in this code. 

## Caveats
As per the Policy for Scientific Data, DFO scientific data are a public resource and subject to full and open access within two years of being acquired or generated. As per the Application for Access to Shellfish Data Holdings and Application for Access to Groundfish Databases, data collected two years prior to today should not be included in data pulls to allow the Principle Investigator a two year period to analyze and report findings. Exceptions to the use of data collected in the last two years may be made if the user has met the conditions outlined in the Application for Access or has discussed the use of the data with the Principle Investigator for the survey.  
This workflow was developed for presence-absence data, but may be adapted for use with density or count data.   
For shellfish surveys observations from years prior to 2010 were filtered out to use more recent data for the development for SDMs. The SQL code can be adapted to include years prior to 2010.   
The SQL code has been developed to extract from the shellfish databases all invertebrate species directly observed, but only some select algae/marine plants. The SQL code can be adapted to include other algae and marine plant species.  
The SQL code does not query ABL data. If you require ABL data you must first notify the SARA program and the DFO Shellfish Data Unit. Following that the relevant SQL code can be updated 

## Uncertainty
Absence records may not accurately represent true absences of the marine species sampled. Absence records were included based on the assumption that groundfish longline surveys and DFO dive surveys had a high enough detectability of the species to accurately represent their absence. Users must consider this assumption carefully before using the data. The assumption about absence data should be revisited by the user as survey objectives may shift over time.

## Acknowledgements
Maria Cornthwaite, Rob Flemming

## References
Nephin, J., Gregr, E.J., St. Germain, C., Fields, C., and Finney, J.L. 2020. Development of a Species Distribution Modelling Framework and its Application to Twelve Species on Canada’s Pacific Coast. DFO Can. Sci. Advis. Sec. Res. Doc. 2020/004. xii + 107 p. https://www.dfo-mpo.gc.ca/csas-sccs/Publications/ResDocs-DocRech/2020/2020_004-eng.html

